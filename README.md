# schedule-bot

Bots that run on a schedule

## Purpose

This repository contains the source code and deployment helpers for all
Python/Bash CKI bots that run on a schedule, i.e. as a cron job. The schedule
itself can be deployed as an OpenShift `CronJob` or a GitLab CI/CD pipeline
schedule.

## Creating a schedule

The [example schedule](schedule-example.yml) should give an idea what a schedule looks like.

| Field       | Type   | Required | Description                                                          |
|-------------|--------|----------|----------------------------------------------------------------------|
| `name`      | string | yes      | schedule name, used to build names for deployed resources            |
| `image`     | string | yes      | container image URL including tag                                    |
| `secrets`   | array  | no       | secret [environment variables] to expose to all jobs in the schedule |
| `variables` | array  | no       | [environment variables] to expose to all jobs in the schedule        |
| `jobs`      | dict   | no       | [scheduled jobs]                                                     |

[scheduled jobs]: #jobs
[environment variables]: #environment-variables

### Jobs

The following fields describe the jobs to run:

| Field        | Type   | Required | Description                                                        |
|--------------|--------|----------|--------------------------------------------------------------------|
| `schedule`   | string | yes      | cron-style schedule for the job (e.g. `0 1 * * *`) ([Cron syntax]) |
| `command`    | array  | (yes)    | Bash command line to run (cannot be combined with `module`)        |
| `module`     | dict   | (yes)    | Python module to run (cannot be combined with `command`)           |
| `image`      | string | no       | container image URL including tag, overrides schedule setting      |
| `variables`  | array  | no       | [environment variables] to expose to this job                      |
| `disabled`   | bool   | no       | ignore this job during deployment                                  |
| `kubernetes` | dict   | no       | [Kubernetes-specific job configuration]                            |
| `gitlab`     | dict   | no       | [GitLab-specific job configuration]                                |
| `config`     | dict   | no       | inline yaml configuration, [environment variables] are replaced    |

[Cron syntax]: https://en.wikipedia.org/wiki/Cron
[Kubernetes-specific job configuration]: #kubernetes-specific-job-configuration
[GitLab-specific job configuration]: #gitlab-specific-job-configuration

### Environment variables

Each job can be customized by specifying common secrets and variables, and
per-job variables. Secrets are stored as masked CI/CD variables (GitLab) or
Kubernetes Secrets (OpenShift).

Both are configured as arrays in one of the following formats:

| Item          | Variable name | Variable value                                        |
|---------------|---------------|-------------------------------------------------------|
| - KEY         | `KEY`         | value of `KEY` environment variable during deployment |
| - KEY: $VAR   | `KEY`         | value of `VAR` environment variable during deployment |
| - KEY: INLINE | `KEY`         | inline value of `INLINE` (not supported for secrets)  |

### Kubernetes-specific job configuration

The `kubernetes` field for a job contains configuration only applicable to a
Kubernetes/OpenShift deployment:

| Field                   | Type   | Required | Description                                                                            |
|-------------------------|--------|----------|----------------------------------------------------------------------------------------|
| `memory`                | string | no       | [Kubernetes memory limits and requests]                                                |
| `cpu`                   | float  | no       | [Kubernetes CPU limits and requests], defaults to 1                                    |
| `concurrencyPolicy`     | string | no       | [Kubernetes Concurrency Policy], defaults to `Replace`                                 |
| `activeDeadlineSeconds` | int    | no       | [Kubernetes Init Containers], on the Pod template, defaults to empty                   |
| `dataVolume`            | string | no       | [Kubernetes Volume], `emptyDir` (default), `memory` or name of `persistentVolumeClaim` |
| `labels`                | dict   | no       | [Kubernetes Labels], additional to `app` and `schedule_job`                            |

[Kubernetes memory limits and requests]: https://kubernetes.io/docs/concepts/configuration/manage-compute-resources-container/#meaning-of-memory
[Kubernetes CPU limits and requests]: https://kubernetes.io/docs/concepts/configuration/manage-compute-resources-container/#meaning-of-cpu
[Kubernetes Concurrency Policy]: https://kubernetes.io/docs/tasks/job/automated-tasks-with-cron-jobs/#concurrency-policy
[Kubernetes Init Containers]: https://kubernetes.io/docs/concepts/workloads/pods/init-containers/#detailed-behavior
[Kubernetes Volume]: https://kubernetes.io/docs/concepts/storage/volumes/
[Kubernetes Labels]: https://kubernetes.io/docs/concepts/overview/working-with-objects/labels/

### GitLab-specific job configuration

The `gitlab` field for a job contains configuration only applicable to a
GitLab deployment:

| Field  | Type  | Required | Description          |
|--------|-------|----------|----------------------|
| `tags` | array | no       | [GitLab runner tags] |

[GitLab runner tags]: https://docs.gitlab.com/ce/ci/yaml/#tags

## Deploying a schedule

Make sure all secrets in the schedule are defined as environment variables, and
then call

```shell
./run.py -c schedule.yml --environment ENVIRONMENT
```

The following environments are supported:

### OpenShift - `openshift`

Deploys a complete schedule as an OpenShift `CronJob`. All resources are
labeled with `app=schedule-bot-SCHEDULE` and
`schedule_job=schedule-bot-SCHEDULE-JOB`. The secrets are deployed as one
common `Secret` resource that is referenced by the individual jobs.

The following additional parameters are supported:

- `--no-cleanup`: optional, do not remove old jobs that are not in the schedule
  anymore
- `--disabled`: optional, disable all jobs independent of the `disabled` settings
  of the schedule

### GitLab - `gitlab`

Deploys a complete schedule as GitLab CI/CD pipeline schedules. The project is
created/reconfigured if needed. The secrets are deployed into masked (if
possible) project variables. The job variables are deployed into schedule
variables. The `.gitlab-ci.yml` CI/CD pipeline configuration is rewritten to
contain one job per schedule.

The following additional parameters are supported:

- `--schedule-project-url`: required, full URL of the GitLab project that will
  hold the CI/CD pipeline schedules
- `--private-token`: required, GitLab personal access token of the owner of the
  CI/CD pipeline schedules
- `--no-cleanup`: optional, do not remove GitLab CI/CD pipeline schedules that
  are not in the schedule anymore

### Podman - `podman`

Runs one job of a schedule immediately with the help of Podman.

The following additional parameters are supported:

- `--job JOB`: required, specifies the job to run immediately
- `--code-overlay DIR`: optional, mount a local directory at /code
- `--image IMAGE`: optional, override the image from the schedule

The job is run in a read-only Podman container with a `tmpfs` in `/data`.

### Local shell - `local`

Runs one job of a schedule immediately in the current shell environment.

The following additional parameters are supported:

- `--job JOB`: required, specifies the job to run immediately

## Available bots

### `git_cache_updater`

Update a cache of tar files containing git repositories in S3.

### `pipeline_bot`

Interact with a user in a merge request for the pipeline definition, and
trigger pipelines for it.

### `orphan_hunter`

Check whether the GitLab jobs for Pods spawned by `gitlab-runner` are still
running. Pods of already finished jobs are deleted.

#### Configuration

| Field           | Type   | Required | Description                                                           |
|-----------------|--------|----------|-----------------------------------------------------------------------|
| `action`        | string | no       | `report` (default) or `delete` Pods                                   |
| `gitlab_tokens` | dict   | yes      | URL/environment variable pairs of GitLab instances and private tokens |

#### Environment variables

| Name                | Secret | Required | Description                                                   |
|---------------------|--------|----------|---------------------------------------------------------------|
| `OPENSHIFT_KEY`     | yes    | yes      | OpenShift credentials                                         |
| `OPENSHIFT_SERVER`  | no     | yes      | OpenShift API server                                          |
| `OPENSHIFT_PROJECT` | no     | yes      | OpenShift namespace                                           |
| `GITLAB_TOKEN`      | yes    | yes      | GitLab private tokens as configured in `gitlab_tokens` above  |
| `IRCBOT_URL`        | no     | no       | IRC bot endpoint                                              |
| `SHORTENER_URL`     | no     | no       | URL shortener endpoint to link to GitLab jobs in IRC messages |

### `datawarehouse_report_crawler`

Submit all sent reports from a mailing list to the [Data Warehouse].

#### Configuration

| Field                 | Type   | Required | Description                                                   |
|-----------------------|--------|----------|---------------------------------------------------------------|
| `action`              | string | no       | `report` (default) or `submit` Reports                        |
| `mailing_list_url`    | string | yes      | URL of mailing list archive                                   |
| `datawarehouse_url`   | string | yes      | URL of Data Warehouse instance                                |
| `datawarehouse_token` | string | yes      | name of environment variable with Data Warehouse secret token |

#### Environment variables

| Name                  | Secret | Required | Description                                                              |
|-----------------------|--------|----------|--------------------------------------------------------------------------|
| `DATAWAREHOUSE_TOKEN` | yes    | yes      | Data Warehouse secret token as configured in `datawarehouse_token` above |

### `kernel_fixes`

Create a `kernel-fixes.json` file as required by the pipeline.

#### Configuration

| Field                | Type   | Required | Description                                                             |
|----------------------|--------|----------|-------------------------------------------------------------------------|
| `kernel.bucket_spec` | string | yes      | name of environment variable with the git cache bucket specification    |
| `kernel.filename`    | string | yes      | file name of the kernel archive in the git cache                        |
| `kernel.max_commits` | int    | no       | number of commits to check for fixes tags, defaults to 10000            |
| `output.bucket_spec` | string | yes      | name of environment variable with the kernel fixes bucket specification |
| `output.filename`    | string | no       | file name of the kernel fixes file, defaults to `kernel-fixes.json`     |

#### Environment variables

| Name                  | Secret | Required | Description                                                                |
|-----------------------|--------|----------|----------------------------------------------------------------------------|
| `BUCKET_GIT_CACHE`    | yes    | yes      | git-cache bucket specification as configured in `kernel.bucket_spec` above |
| `BUCKET_KERNEL_FIXES` | yes    | yes      | output bucket specification as configured in `output.bucket_spec` above    |

### `git_s3_sync`

Sync a git repository to an S3 bucket.

#### Environment variables

| Name                 | Secret | Required | Description                                                   |
|----------------------|--------|----------|---------------------------------------------------------------|
| `BUCKET_CONFIG_NAME` | no     | yes      | name of environment variable with the bucket specification    |
| `BUCKET_CONFIG`      | yes    | yes      | bucket specification as configured in `BUCKET_CONFIG_NAME`    |
| `REPO_URL`           | no     | yes      | git repository to sync                                        |
| `REPO_SUBDIR`        | no     | no       | subdirectory of the git repository to sync, defaults to empty |

### `pgsql_backup`

Backup a PostgreSQL database to an S3 bucket.
A restore script is provided that reconstructs an SQL dump from the last full or incremental backup.

#### Environment variables

| Name                     | Secret | Required | Description                                                              |
|--------------------------|--------|----------|--------------------------------------------------------------------------|
| `BUCKET_CONFIG_NAME`     | no     | yes      | name of environment variable with the bucket specification               |
| `BUCKET_CONFIG`          | yes    | yes      | bucket specification as configured in `BUCKET_CONFIG_NAME`               |
| `PG_USERNAME`            | no     | yes      | PostgreSQL user name                                                     |
| `PG_PASSWORD`            | yes    | yes      | PostgreSQL password                                                      |
| `PG_HOST`                | no     | yes      | PostgreSQL host                                                          |
| `PG_DB`                  | no     | yes      | PostgreSQL database                                                      |
| `BACKUP_FULL_MAX`        | no     | no       | maximum number of full backups to keep, defaults to 7                    |
| `BACKUP_INCREMENTAL_MAX` | no     | no       | number of incremental backups until a full backup is made, defaults to 7 |

### `example_shell_bot`

Show defined environment variables and test whether the data directory is
writable.

## Development

### Environment variables provided to the bots

- `SCHEDULE_BOT_CONFIG`: JSON job configuration from the schedule
- `SCHEDULE_BOT_DATA_DIR`: writable temporary directory
- `SCHEDULE_BOT_JOB_NAME`: name of the scheduled job

### Locally building the schedule-bot image

```shell
podman run \
    -v .:/code \
    -w /code \
    -e IMAGE_NAME=schedule-bot \
    --privileged \
    -v ~/.local/share/containers:/var/lib/containers \
    registry.gitlab.com/cki-project/containers/buildah:latest \
    cki_build_image.sh
```

### Running a job locally

A job can be run immediately on the local machine in two environments: in the
current shell environment (`local`) or in a Podman container (`podman`).

As an example, this would launch the `git-cache-updater job` of `schedule.yml`
in a Podman container, running code from the current working directory and
using the `schedule-bot` image from the local host:

```shell
./run.py -c schedule.yml \
    --environment podman \
    --job git-cache-updater \
    --code-overlay . \
    --image localhost/schedule-bot
```

### OpenShift

To manually run a job for a schedule, clone the `CronJob` into a normal `Job`
with something like

```shell
oc create job --from cronjob/schedule-bot-SCHEDULE-JOB schedule-bot-JOB-01
```

To delete everything, run

```shell
oc delete all -l app=schedule-bot
```

[Data Warehouse]: https://gitlab.com/cki-project/datawarehouse
