# Testing the deployment of the schedule bot

The deployment of the schedule bot has integration tests that must be run before merging any changes.

To run them, you need to have access to another https://gitlab.com account that
is a member of the CKI team.
Then, define the following environment variables:

- `IT_DEPLOY_GITLAB_URL`: https://gitlab.com
- `IT_DEPLOY_GITLAB_TOKEN_MEMBER`: your personal access token at https://gitlab.com
- `IT_DEPLOY_GITLAB_TOKEN_CI_BOT_MEMBER`: a personal access token for the member bot
- `IT_DEPLOY_GITLAB_PARENT_PROJECT`: cki-project

For OpenShift, make sure you are logged in with `oc login` before running the tests.

You can run the integration tests with

```shell
# run all tests
python3 -m unittest inttests.test_schedule_bot
# run an individual test
python3 -m unittest inttests.test_schedule_bot.TestScheduleBot.test_gitlab -v
```
