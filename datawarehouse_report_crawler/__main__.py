"""Datawarehouse report crawler."""
import datetime
import json
import logging
import os

from . import helpers
from .classes import LatestMailArchiveCrawler
from .classes import MMessageBase


def filter_messages(messages_list):
    """Remove unwanted messages."""
    messages = []

    for message in messages_list:
        # Filter out messages without pipeline id
        if not message.pipelineid:
            continue

        # Not a result email
        if not (
                'SKIPPED: ' in message.msg['Subject'] or
                'PASS: ' in message.msg['Subject'] or
                'PANICKED: ' in message.msg['Subject'] or
                'FAIL: ' in message.msg['Subject']):
            continue

        # Filter out messages older than 7 days.
        message_datetime = datetime.datetime.strptime(
            message.msg['X-List-Received-Date'],
            '%a, %d %b %Y %H:%M:%S %z'
        ).replace(tzinfo=None)
        last_week = datetime.datetime.now() - datetime.timedelta(days=7)
        if message_datetime < last_week:
            continue

        # Fallback. Add to the list.
        messages.append(message)

    return messages


def process_message(datawarehouse_url, datawarehouse_token, message, action):
    """Process the email and report to DW if necessary."""
    try:
        reports = helpers.get_pipeline_reports(datawarehouse_url,
                                               message.pipelineid)
    except Exception:  # pylint: disable=broad-except
        logging.error("Couldn't find pipeline %d in the DW",
                      message.pipelineid)
        return

    if not reports:
        if action == 'submit':
            helpers.submit_report(datawarehouse_url, datawarehouse_token,
                                  message.pipelineid, message.msg)
        else:
            print(f'Report for {message.pipelineid} should be submitted')


def main(config):
    """Run, Forest, run!."""
    cookie_path = os.path.join(os.environ['SCHEDULE_BOT_DATA_DIR'], 'cookies')
    cache_path = os.path.join(os.environ['SCHEDULE_BOT_DATA_DIR'], 'cache')

    datawarehouse_url = config['datawarehouse_url']
    datawarehouse_token = os.environ[config['datawarehouse_token']]
    action = config.get('action', 'report')

    crawler = LatestMailArchiveCrawler(config['mailing_list_url'],
                                       None, cookie_path)
    crawler.update_listarchive(cache_path)

    messages = MMessageBase.listarchives2msgs(cache_path)
    messages = filter_messages(messages)

    for message in messages:
        process_message(datawarehouse_url,
                        datawarehouse_token, message, action)


main(json.loads(os.environ['SCHEDULE_BOT_CONFIG']))
