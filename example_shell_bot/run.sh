#!/bin/bash
set -euo pipefail

# show exported environment variables
export | sed 's/=.*//'

# test whether data dir is writable
echo "dummy" > "${SCHEDULE_BOT_DATA_DIR}/test.txt"
