"""Python test helper to check env variables and data directory."""
import os

for index in range(10):
    print(f'+Var{index}: {os.getenv(f"Var{index}", "empty")}')
try:
    with open(os.getenv('SCHEDULE_BOT_DATA_DIR') + '/test.txt', 'w') as f:
        f.write('test')
    print('+Write: ok')
except Exception:  # pylint: disable=broad-except
    print('+Write: error')
