#!/usr/bin/python3
"""Test the deployment of the schedule bot."""
import datetime
import json
import os
import subprocess
import time
import unittest
import uuid
from unittest import mock

import dateutil.parser
import gitlab
import yaml

from schedule_bot import utils
from schedule_bot.gitlab import GitLabDeployment
from schedule_bot.local import LocalDeployment
from schedule_bot.openshift import OpenShiftDeployment
from schedule_bot.podman import PodmanDeployment


@mock.patch.dict(os.environ, {'Var0': 'value0',
                              'ValueVar1': 'value1',
                              'Var2': 'value2',
                              'ValueVar3': 'value3',
                              'Var5': 'value5',
                              'ValueVar6': 'value6',
                              })
class TestScheduleBot(unittest.TestCase):
    """Test the deployment of the schedule bot."""

    _env = {'Var8': 'value8'}

    def test_local(self):
        """Check that direct deployment works."""
        for job in ('shell', 'python'):
            deployment = LocalDeployment(self._config(), self._env, job)
            with utils.stdout_redirected() as output:
                deployment.deploy()
            self._check_output(output.read())

    def test_podman(self):
        """Check that direct deployment via Podman works."""
        for job in ('shell', 'python'):
            deployment = PodmanDeployment(self._config(), self._env,
                                          job, code_overlay='.')
            with utils.stdout_redirected() as output:
                deployment.deploy()
            self._check_output(output.read())

    def test_gitlab(self):
        """Check that deployment as a GitLab pipeline schedule works."""
        instance_url = os.environ['IT_DEPLOY_GITLAB_URL']

        token_bot = os.environ['IT_DEPLOY_GITLAB_TOKEN_CI_BOT_MEMBER']
        gl_bot = gitlab.Gitlab(instance_url, token_bot)
        gl_bot.auth()

        token_member = os.environ['IT_DEPLOY_GITLAB_TOKEN_MEMBER']
        gl_member = gitlab.Gitlab(instance_url, token_member)
        gl_member.auth()

        project_path = os.path.join(
            os.environ['IT_DEPLOY_GITLAB_PARENT_PROJECT'],
            f'inttest-{uuid.uuid4()}')
        project_url = os.path.join(instance_url, project_path)
        try:
            # schedule jobs
            deployment = GitLabDeployment(self._config(), self._env, False,
                                          project_url,
                                          token_member)
            deployment.deploy()
            deployment.cleanup()
            project = gl_member.projects.get(project_path)
            now_plus_5_60 = datetime.datetime.now(datetime.timezone.utc) + \
                datetime.timedelta(minutes=5, seconds=60)
            for schedule in project.pipelineschedules.list(as_list=False):
                self.assertGreater(now_plus_5_60,
                                   dateutil.parser.parse(schedule.next_run_at))
                schedule.active = False
                schedule.save()
                # /play this is not yet supported in the Python API
                gl_member.http_post(
                    f'{schedule.manager.path}/{schedule.get_id()}/play')
            # enable the schedule again + transfer ownership
            deployment = GitLabDeployment(self._config(), self._env, False,
                                          project_url,
                                          token_bot)
            deployment.deploy()
            deployment.cleanup()
            for schedule in project.pipelineschedules.list(as_list=False):
                self.assertEqual(schedule.active, True)
                self.assertEqual(schedule.owner['id'], gl_bot.user.id)
                schedule.active = False
                schedule.save()
            # wait for jobs to finish
            for _ in range(10):
                jobs = project.jobs.list()
                if len(jobs) == 2 and all(j.finished_at for j in jobs):
                    break
                time.sleep(20)
            # check job output
            self.assertEqual(len(jobs), 2)
            for job in jobs:
                self._check_output(job.trace().decode('utf-8'))
        finally:
            with utils.only_log_exceptions():
                gl_member.projects.get(project_path).delete()

    def test_openshift(self):
        """Check that deployment as an OpenShift CronJob works."""
        try:
            # schedule jobs
            deployment = OpenShiftDeployment(self._config(), self._env, False)
            deployment.deploy()
            deployment.cleanup()
            # wait for jobs to finish
            for _ in range(10):
                output = self._oc(['get', 'job', '-o', 'json',
                                   '-l', 'app=schedule-bot-inttest'])
                items = json.loads(output)
                if len(items['items']) == 2 and \
                        all(self._oc_finished(j) for j in items['items']):
                    break
                time.sleep(20)
            # check job output
            self.assertEqual(len(items['items']), 2)
            for job in items['items']:
                output = self._oc(['logs', f'job/{job["metadata"]["name"]}'])
                self._check_output(output)
        finally:
            for resource in ('all', 'secret'):
                with utils.only_log_exceptions():
                    self._oc(['delete', resource,
                              '-l', 'app=schedule-bot-inttest'])

    @staticmethod
    def _oc_finished(job):
        return job.get('status', {}).get('succeeded', 0) == 1

    @staticmethod
    def _oc(args):
        return subprocess.run(['oc'] + args, capture_output=True,
                              encoding='utf-8', check=True).stdout

    @staticmethod
    def _config():
        tag = 'latest'
        image = 'registry.gitlab.com/cki-project/schedule-bot/schedule-bot'
        # minute rounding and 60 seconds for deployment
        delay = datetime.timedelta(minutes=1, seconds=60)
        scheduled = datetime.datetime.utcnow() + delay
        schedule = f'{scheduled.minute} {scheduled.hour} * * *'
        return {
            'name': 'inttest', 'image': f'{image}:{tag}',
            'secrets': [
                'Var0',
                {'Var1': '$ValueVar1'},
            ],
            'variables': [
                'Var5',
                {'Var6': '$ValueVar6'},
                {'Var7': 'value7'},
                'Var8',
            ],
            'jobs': {
                'shell': {
                    'schedule': schedule,
                    'command': 'inttests/shell/run.sh',
                    'variables': [
                        'Var2',
                        {'Var3': '$ValueVar3'},
                        {'Var4': 'value4'},
                    ]
                },
                'python': {
                    'schedule': schedule,
                    'module': 'inttests.python',
                    'variables': [
                        'Var2',
                        {'Var3': '$ValueVar3'},
                        {'Var4': 'value4'},
                    ]
                }
            }
        }

    def _check_output(self, log):
        filtered = [s[1:] for s in log.split('\n') if s.startswith('+')]
        output = yaml.safe_load('\n'.join(filtered))
        for index in range(9):
            self.assertEqual(output[f'Var{index}'], f'value{index}')
        self.assertEqual(output['Write'], 'ok')
