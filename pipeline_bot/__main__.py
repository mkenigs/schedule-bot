"""Interface to python -m for the pipeline trigger."""
import json
import os

from . import main

main(json.loads(os.environ['SCHEDULE_BOT_CONFIG']))
