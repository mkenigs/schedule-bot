"""Main module to deploy a bot schedule on OpenShift or GitLab."""
import argparse

import yaml

from .gitlab import GitLabDeployment
from .local import LocalDeployment
from .openshift import OpenShiftDeployment
from .podman import PodmanDeployment


def main():
    """Start the appropriate command."""
    parser = argparse.ArgumentParser(
        description='Deploy a bot schedule.')
    parser.add_argument('--environment',
                        choices=('openshift', 'gitlab', 'podman', 'local'),
                        default='openshift',
                        help='target environment for the deployment')
    parser.add_argument('--config', '-c', required=True,
                        type=argparse.FileType('r'),
                        help='YAML configuration file to use')
    parser.add_argument('--job',
                        help='job to launch (only with Podman)')
    parser.add_argument('--code-overlay',
                        help='mount a directory at /code instead of using ' +
                        'it from the container (only with Podman)')
    parser.add_argument('--image',
                        help='override the container image (only with Podman)')
    parser.add_argument('--no-cleanup',
                        help='do not remove old jobs that are not in the ' +
                        'schedule anymore (only gitlab and openshift)')
    parser.add_argument('--schedule-project-url',
                        help='URL of the project on GitLab that will hold ' +
                        'the schedules (only gitlab)')
    parser.add_argument('--private-token',
                        help='GitLab personal access token of the owner of ' +
                        'the CI/CD pipeline schedules (only gitlab)')
    parser.add_argument('--disabled', action='store_true',
                        help='Disable the schedule')
    parser.add_argument('--env', '-e', action='append', default=[],
                        help='Set extra key=value environment variables')
    args = parser.parse_args()

    config = yaml.safe_load(args.config)
    env = dict(e.split('=', 1) for e in args.env)

    if args.environment == 'openshift':
        deployment = OpenShiftDeployment(config, env, args.disabled)
    elif args.environment == 'gitlab':
        deployment = GitLabDeployment(config, env, args.disabled,
                                      args.schedule_project_url,
                                      args.private_token)
    elif args.environment == 'podman':
        deployment = PodmanDeployment(config, env, args.job,
                                      image=args.image,
                                      code_overlay=args.code_overlay)
    elif args.environment == 'local':
        deployment = LocalDeployment(config, env, args.job)
    else:
        raise Exception('Unsupported deployment environment')
    deployment.deploy()
    if hasattr(deployment, 'cleanup') and not args.no_cleanup:
        deployment.cleanup()


main()
