"""Immediate local deployment of one job in a schedule."""
import os
import subprocess
import tempfile

from . import utils


class LocalDeployment(utils.Deployment):
    """Local testing of one job in a schedule."""

    def __init__(self, schedule_config, extra_env_variables, job):
        """Prepare the local deployment of one job in a schedule."""
        super().__init__(schedule_config, extra_env_variables)
        self._job_name = job
        self._job_config = schedule_config['jobs'][job]

    def deploy(self):
        """Locally deploy one job in a schedule."""
        with tempfile.TemporaryDirectory() as data_dir:
            variables = dict(os.environ)
            variables.update(self.schedule_variables())
            variables.update(self.schedule_secrets())
            variables.update(self.job_variables(
                self._job_name, data_dir=data_dir))
            subprocess.run(self.command_line(self._job_name),
                           check=True, env=variables)
