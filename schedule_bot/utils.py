"""Common schedule bot helpers."""
import collections
import contextlib
import copy
import io
import json
import os
import sys
import tempfile
import traceback


@contextlib.contextmanager
def only_log_exceptions(exceptions=Exception):
    """Log, but ignore exceptions."""
    try:
        yield
    except exceptions:  # pylint: disable=broad-except
        traceback.print_exc()


def cleanup_gitlab(deletable_objects):
    """Remove merge requests, branches and projects from a GitLab instance."""
    for deletable_object in deletable_objects:
        with only_log_exceptions():
            deletable_object.delete()


@contextlib.contextmanager
def stdout_redirected():
    """Redirect stdout and return as io.StringIO."""
    result = io.StringIO()
    with tempfile.TemporaryFile() as temp_file:
        stdout_fd = sys.stdout.fileno()
        # make a copy of stdout...
        with os.fdopen(os.dup(stdout_fd), 'wb') as copied:
            sys.stdout.flush()
            os.dup2(temp_file.fileno(), stdout_fd)  # redirect
            try:
                yield result
            finally:
                sys.stdout.flush()
                # ...and restore it again
                os.dup2(copied.fileno(), stdout_fd)
                temp_file.seek(0)
                result.write(temp_file.read().decode('utf-8'))
                result.seek(0)


class Deployment:
    """Base class for the deployment of a schedule."""

    def __init__(self, schedule_config, extra_env_variables, disabled=False):
        """Prepare the deployment of a schedule."""
        self._schedule_config = schedule_config
        self._environ = dict(os.environ)
        self._environ.update(extra_env_variables)
        self._disabled = disabled

    def command_line(self, job_name):
        """Get a single command line array to run the job."""
        job_config = self._schedule_config['jobs'][job_name]
        if 'module' in job_config:
            return ['python3', '-m', job_config['module']]
        if 'command' in job_config:
            return ['bash', '-c', 'set -o pipefail && ' +
                    job_config['command']]
        raise Exception('Shell command or Python module required')

    def command_lines(self, job_name):
        """Get multiple command strings to run the job."""
        job_config = self._schedule_config['jobs'][job_name]
        if 'module' in job_config:
            return [f'python3 -m {job_config["module"]}']
        if 'command' in job_config:
            return ['set -o pipefail', job_config['command']]
        raise Exception('Shell command or Python module required')

    def job_variables(self, job_name, data_dir='/data'):
        """Get the common env variables from the job configuration."""
        job_config = self._schedule_config['jobs'][job_name]
        result = self._variable_dict(job_config.get('variables', []), True)
        result['SCHEDULE_BOT_JOB_NAME'] = job_name
        config = copy.deepcopy(job_config.get('config'))
        self.resolve_variables(config)
        result['SCHEDULE_BOT_CONFIG'] = json.dumps(config)
        if data_dir:
            result['SCHEDULE_BOT_DATA_DIR'] = data_dir
        return result

    def schedule_secrets(self):
        """Return a dict with the mapped secrets."""
        return self._variable_dict(
            self._schedule_config.get('secrets', []), False)

    def schedule_variables(self):
        """Return a dict with the mapped variables."""
        return self._variable_dict(
            self._schedule_config.get('variables', []), True)

    def _variable_dict(self, variables, allow_inline):
        source_vars = set(Deployment._variable_source(v) for v in variables)
        unknown_vars = source_vars - set((None,)) - set(self._environ)
        if unknown_vars:
            raise Exception('The following required environment variables are '
                            f'undefined: {", ".join(unknown_vars)}')
        return dict(self._variable_item(v, allow_inline)
                    for v in variables)

    def resolve_variables(self, config):
        """Recursively resolve variables in nested dicts and lists."""
        if isinstance(config, dict):
            self._resolve_items(config, config.items())
        elif isinstance(config, list):
            self._resolve_items(config, enumerate(config))

    def _resolve_items(self, config, items):
        for key, value in items:
            self.resolve_variables(config[key])
            if isinstance(value, str) and value.startswith('$'):
                if value.startswith('${'):
                    var_name = value[2:-1]
                else:
                    var_name = value[1:]
                if var_name not in self._environ:
                    raise Exception(
                        f'The {var_name} environment variable is undefined')
                config[key] = self._environ[var_name]

    def _variable_item(self, variable, allow_inline):
        if isinstance(variable, str):
            return variable, self._environ[variable]
        key, value = next(iter(variable.items()))
        if value.startswith('${'):
            return (key, self._environ[value[2:-1]])
        if value.startswith('$'):
            return (key, self._environ[value[1:]])
        if not allow_inline:
            raise Exception(f'Inline variables not allowed for secrets: {key}')
        return key, value

    @staticmethod
    def _variable_source(variable):
        if isinstance(variable, str):
            return variable
        value = next(iter(variable.values()))
        if value.startswith('${'):
            return value[2:-1]
        if value.startswith('$'):
            return value[1:]
        return None

    def image(self, job_name):
        """Return the container image to use for a job."""
        return (self._schedule_config['jobs'][job_name].get('image') or
                self._schedule_config['image'])


# pylint: disable=too-few-public-methods
class Colors:
    """ANSI escape sequences for color output."""

    RED = '\033[1;31m'
    GREEN = '\033[1;32m'
    YELLOW = '\033[1;33m'
    END = '\033[0m'


def cprint(color: Colors, *args, **kwargs):
    """Print colored messages."""
    print(*(f'{color}{str(a)}{Colors.END}' for a in args), **kwargs)


BucketSpec = collections.namedtuple('BucketSpec',
                                    ['endpoint', 'access_key', 'secret_key',
                                     'bucket', 'prefix'])


def parse_bucket_spec(bucket_spec) -> BucketSpec:
    """Parse a deployment-all-style bucket specification."""
    endpoint, access_key, secret_key, bucket, bucket_path = bucket_spec.split(
        '|')
    endpoint = endpoint.rstrip('/') or 'http://s3.amazonaws.com'
    bucket = bucket.rstrip('/')
    bucket_path = bucket_path.rstrip('/') + '/' if bucket_path else ''
    return BucketSpec(endpoint, access_key, secret_key, bucket, bucket_path)
